#2 – Mehr def
#Definiere eine Funktion, die drei Zahlen übernimmt und ausgibt, ob die dritte Zahl die Summe der beiden ersten ist.


zahl1 = 7
zahl2 = 9
zahl3 = 15

def ist_das_die_summe(summand1, summand2, summe): #Hier bewusst andere Namen verwendet! Das ist erstmal die Definition der Argumente (=Schachteln) innerhalb der Funktion. Variablen setzt man beim _Benutzen_ der Funktion ein, nicht beim Definieren.
    if summand1 + summand2 == summe: #kürzere Variante: return summand1 + summand2 == summe, da kommt schon von alleine (durch die Prüfung von ==) True oder False raus
        return True
    else:
        return False


print(ist_das_die_summe(zahl1, zahl2, zahl3)) #diese Prüfungsfunktion soll True/False ergeben

#Mache eine zweite Version des Mathe-Übungsprogramms vom letzten Mal, in der Du die Funktion verwendest! (Kopiere die Datei und ersetze nur die Stelle.)

import random

ANZAHL_VERSUCHE = 3
ANZAHL_AUFGABEN = 4
SCHWIERIGKEITSGRAD = 10

punkte = 0


for j in range(ANZAHL_AUFGABEN):
    rand1 = random.randint(1, SCHWIERIGKEITSGRAD)
    rand2 = random.randint(1, SCHWIERIGKEITSGRAD)
    inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)


    for i in range(ANZAHL_VERSUCHE):
        answer = input(inputfrage)
        answer = int(answer)
        if ist_das_die_summe(rand1,rand2,answer): #hier wurde die Funktion eingefügt, an anderen Stellen geht das gar nicht
            print("Richtig")
            punkte = punkte + 1
            break
        else:
            if i == ANZAHL_VERSUCHE - 1:
                print("Falsch! Das war dein letzter Versuch!")
                break
            if abs(answer - (rand1+rand2)) == 1:
                print("Du liegst nur um 1 daneben!")
            print("Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i + 1)))


    print("Aktueller Punktestand: " + str(punkte))
