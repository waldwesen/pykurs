# Pikos Python Kurs am 14.11.2021
vier Stunden Workshop



## Das Pad

/, %, // und Teilen mit Rest
~~~
* 33%5, was bedeutet %?, warum kommt 3 heraus?
33/5 = 30/5 R3 = 6 R3
44/8 = 40/8 R4 = 5 R4

4/3 = 1,33333333
4/3 = 1 R1
7/3 = 2 R

322/100 = 3 R22

322%100 = 22
322//100 = 3

/
44//8 = 5
44%8  = 4

44/8 = (44//8) R(44%8)
~~~

Tupel, Plus
~~~
* 13 + 18.0 = 31.0 (=ist ein integer, no?)
32 und mach einmal Komma => 3.2
12345 und mach viermal Komma => 1.2345
"a" + 3
5 + 3 = 8
"a" + "b" = "ab"

float(int(13 + 18.0))
~~~
* Was ist dann das Ergebnis in der Klammer, wenn ein Komma hinter der 18 steht? 13 + 18,0 (= (31,0))
~~~
a = 18.0
b = 18,0
type(a)

# Listen sind veränderlich
liste1 = ["apfel", "birne"]
liste1.append("kiwi")

# Tupel sind unveränderlich, die werden neu erzeugt
tupel1 = ("apfel", "birne")
tupel1 = (tupel1[0], tupel1[1], "kiwi")

# 13 + 18,0 (= (31,0))
# 13+18     ,  0 

~~~
### Import Statements


#### Variante 1 - dirty
~~~
from math import *
sqrt(100)
~~~

#### Variante 2 - professioneller
~~~
import math
math.sqrt(100) 
~~~

## Pikos Python-Shell
~~~
>>> fd(20) or True
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'fd' is not defined
>>> True or fd(20)
True
>>> True or False
True
>>> False or True
True
~~~
Python ist faul und wertet nur das aus, was es wirklich auswerten muss. Von links nach rechts in diesem Fall!


~~~
>>> result(a, b) = a + b
  File "<stdin>", line 1
    result(a, b) = a + b
    ^
SyntaxError: cannot assign to function call
~~~
Funktionen sind keine Schachteln, in denen man etwas lagern kann. Variablen sind Schachteln.

Funktionen vs. Variablen:
~~~
>>> 3 + 6
9
>>> max(3, 6)
6
~~~ 
Funktionen geben etwas aus. Ausdrücke (zB 3 + 6) auch.  

Variablen speichern etwas:
~~~
>>> result = 3 + 6
>>> result
9
>>> result = 3 + 5
>>> result
8
~~~

Variablen können auch Ausgaben einer Funktion speichern:
~~~
>>> grosse_zahl = max(3,4,28,5,7)
>>> grosse_zahl
28
>>> kleine_zahl = 1
~~~
Auch "or" und "not" sind Teile von Ausdrücken, die ausgewertet werden, nämlich boole'sche Ausdrücke.  
Die werden dann auf True oder False ausgewertet:
~~~
>>> wahrheitswert = not 10 or 12
>>> wahrheitswert
12
>>> wahrheitswert = not 10 or True
>>> wahrheitswert
True
>>> 8 + 3
11
>>> eine_zahl = 8 + 3
~~~
Leerzeichen sind wichtig: "orTrue" könnte eine Variable sein; "or True" ist Teil eines boolean Ausdrucks.
~~~
>>> orTrue = 20
~~~
"None" ist ein "leerer Wert"
~~~
>>> nichts = None
>>> nichts
>>> print(None)
None
~~~


Verwendung von import:
~~~
>>> import math
>>> sqrt(9)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'sqrt' is not defined
>>> math.sqrt(9)
3.0
>>> from math import *
>>> sqrt(9)
3.0
>>> import turtle
>>> turtle.fd(100)
>>> zahl = 7
>>> stimmung = 7
>>> if stimmung < 5:
...     print("Iss mal mehr Schokolade!")
... 
>>> stimmung = 3
>>> if stimmung < 5:
...     print("Iss mal mehr Schokolade!")
... 
Iss mal mehr Schokolade!

~~~