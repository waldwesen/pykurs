# Beispiele für die Stunde am 04.11.2021

import math
# Hausaufgabenbesprechung
# Aufgabe 2


def zaehle_buchstabe(text, buchstabe):  # bsp: Text, "e"
    """zaehle alle Vorkommen von buchstabe in text"""
    anzahl = 0
    for zeichen in text:  # zeichen geht jedes Zeichen von text durch
        if zeichen == buchstabe:  # wenn zeichen das Gleiche ist wie Buchstabe, im Bsp: "e"
            anzahl = anzahl + 1  # anzahl wird um 1 erhöht
    return anzahl

# print(zaehle_buchstabe("Hallo ich bin ein Pythonkurs und heute ganz schön.", "e"))


def prim(zahl):
    """teste, ob eine ganze Zahl eine Primzahl ist"""
    liste = [2, 3, 5]
    if zahl < 2:  # keine Primzahl kleiner zwei
        return False
    elif zahl in liste:  # Liste ist mit Primzahlen
        return True
    for t in range(2, int(math.sqrt(zahl) + 1)):  # t ist alles von 2 bis Wurzel der Zahl
        if (zahl % t) == 0:  # dann wäre ja zahl durch t teilbar
            return False
    return True

def tolle_funktion(var1):
    ...

primliste100 = []

for m in range(100):
    if prim(m): # wenn prim(m) true
        primliste100.append(m)


def primfaktorliste(zahl):
    """gebe die Liste der Primzahlen aus, durch die diese Zahl teilbar ist"""
    liste = []
    for i in primliste100:  # gehe alle Primzahlen bis 100 durch
        while zahl % i == 0:  # dann ist zahl durch i teilbar
            zahl = zahl/i
            liste.append(i)
    return liste

# print(primfaktorliste(128))


code = "mrnb rbc enablquübbnuc"

alphabet = "abcdefghijklmnopqrstuvwxyz"
alphabet = alphabet + alphabet

def rotieren(wort, zahl):
    """printe für einen String die Caesar-Verschiebung um zahl"""
    ausgabe = ""
    for buchstabe in wort:
        if buchstabe in alphabet:
            # hänge den Buchstaben an, der um zahl im Alphabet hinter buchstabe kommt:
            # Hier
            # sehr
            # lange
            # Kommentare
            ausgabe = ausgabe + alphabet[alphabet.index(buchstabe) + zahl]
        else:  # wenn Satzzeichen oder Leerzeichen
            ausgabe = ausgabe + buchstabe
    print(ausgabe)

for i in range(26):
    rotieren(code, i)

