# Aufgaben am 05.10.2021


# 0 – Lesestoff
## Programmer humor
https://www.smbc-comics.com/comic/the-programhttps://md.ha.si/hSddjptRQKGKXFzatiQGRQ#

https://chaos.social/system/media_attachments/files/107/045/101/125/367/029/original/7232c39b77da700b.jpg

## Aussagenlogik
Das erste Kapitel hiervon: (mit Übungen!)
https://www.springer.com/de/book/9783834899897
(Fand sich auch in den Büchern, die Springer im Frühjahr 2020 kostenlos zum Download angeboten hat)
(Hier wird eventuell noch ein Link auf eine gute Ressource nachgeliefert. Piko ist auf der Suche.)

## Pseudorandom Lesestoff
https://de.wikipedia.org/wiki/Pseudozufall


# 1 – input
(Aufgabenidee von Nicki)
Schreibe ein Programm, das nach einer Zahl fragt und dann ein buntes Vieleck mit so vielen Ecken malt. Jede Kante soll eine neue Farbe bekommen.


# 2 – Random
Baue ein Programm, das folgendes tut und ausgibt:
```
Erste Zufallszahl: 8
Zweite Zufallszahl: 13
Summe der beiden Zahlen: 21
```
Die Zahlen sollen natürlich zufällig sein und nicht jedesmal 8 und 13.

# 3 – Mathe-Übungsprogramm
## 3.1 – Prüfung
Schreibe ein Programm, das nach der Summe zweier zufälliger Zahlen fragt und die Korrektheit des Ergebnisses überprüft.

## 3.2 – Schleifen
Packe die Frage in eine for-Schleife, sodass man drei Versuche hat.
Packe das Ganze dann in eine weitere for-Schliefe, sodass man mehrere Aufgaben bekommt.
Lass die Punktzahl mitzählen und am Ende ausgeben.
