# Pikos Python Kurs 12.10.2021

## waldwesens Protokoll

### Organisatorisches
* Neue Personen dürfen gerne dazu kommen, müssen keine Haecksen sein, aber FLINTA
* Gruppen-Pad: https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?view# gerne zur Selbstorganisation nutzen!
* Tut euch in Gruppen zusammen, da ist die Motivation viel leichter, dabei zu bleiben
* bei allen Problemen schreibt Piko eine Mail Python@ithea.de
* Protokolle und Hausaufgaben bei Gitlab: https://gitlab.com/sudo_piko/pykurs/-/tree/main
* Gerne in Python-Mailingliste eintragen wenn nicht schon geschehen: https://lists.haecksen.org/listinfo/python
* Poll: Der Kurs wird wahrscheinlich auf Mittwoch oder Donnerstag welchseln müssen, was wäre möglich?
* es sieht eher nach Donnerstag aus, es kann sein dass das schon der Termin am 26.10. auf den 28.10. verschoben werden muss
* Infos kommen auf jeden Fall nochmal über die Mailingliste!
* Poll: kommen die Mails über die Mailingliste an? - das sieht sehr gut aus, 94% "Ja"

### Spaß
XKCD ist eine tolle Comic-Seite! Lernt sie auswendig! https://xkcd.com/353/

### Aufgaben

### Aufgabe 1
```
from turtle import *
import random #das random modul importieren

ecken = int(input("Wieviele Ecken dürfen es denn sein? ")) #ausführende person kann dann Ecken angeben

colormode(255) #das sorgt dafür dass man mit RGB-Farbcodes (0-255) arbeiten kann (sonst gehen Farbcodes nur von 1-10)
pensize(10)
for i in range(ecken):
    pencolor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    fd(20) #20 nach vorne
    lt(360/int(ecken)) #um einen Kreisanteilswinkel nach links drehen

exitonclick() #
```

### Lösung ohne random

```
from turtle import *

ecken = int(input("Wieviele Ecken dürfen es denn sein? "))

farben = ["red", "green", "blue"]

while len(farben) < ecken: #so lange es weniger farben als ecken gibt, verdopple einfach die Farben-Liste
    farben = farben + farben

pensize(10)
for i in range(ecken):
    pencolor(farben[i]) #nimm einfach die i-te Position aus der Farbenliste
    fd(20)
    lt(360/int(ecken))

exitonclick()

#Wer Spaß an Rätseln hat darf überlegenwie man aus dem Vieleck einen Stern macht
```

### Aufgabe 2
```
import random
Erste = random.randint(-100,100)
Zweite = random.randint(-100,100)
Summe = Erste + Zweite
print ('Die erste Zahl lautet ', Erste, ', die zweite', Zweite, ', die Summe ist', Summe)
```

### Aufgabe 3.1 Lottes Lösung (durch Piko optimiert)

```
import random

rand1 = random.randint(1, 20) #Zufallszahlen setzen
rand2 = random.randint(1, 20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)
answer = input(inputfrage) # Fragt den User* nach einem input
answer = int(answer) #aus answer wird der integer-Wert

if answer == rand1 + rand2: #das hier ist ein Vergleich, keine Veränderung mehr von answer
    print("Richtig")

else:
    print("Falsch")
```
### Aufgabe 3.2
```
import random
ANZAHL_VERSUCHE = 3 #das ist eine Kostante, deshalb schreibt man sie oben und groß!
rand1=random.randint(1,20)
rand2=random.randint(1,20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2) #das reicht wenn es 1 Mal passiert
for i in range(ANZAHL_VERSUCHE):
    answer = input(inputfrage)  # Fragt den User* nach einem input
    answer = int(answer)  # aus answer wird der integer-Wert

    if answer == rand1 + rand2:  # das hier ist ein Vergleich, keine Veränderung mehr von answer
        print("Richtig")
        break

    else:
        if i == ANZAHL_VERSUCHE - 1:
            print("Falsch, das war dein letzter Versuch :P")
            break
        print("Falsch, versuche es noch einmal. Anzahl verbleibender Versuche:" + str(ANZAHL_VERSUCHE - (i+1)))

# Hausaufgabe: Wie bekommt man drei verschiedene Aufgaben hin? Und wie zählt man da die Punkte mit?
```



### Schlussbemerkungen
* Lest euch Fehler in Ruhe durch, es ist wichtig Fehler kennen zu lernen und wie man sie löst
* Zum Lösen von Fehlern kann man zwischendrin print statements rein tun, z.B. um sich zwischendurch eine Variable auszugeben

### Stackoverflow
* Sortieren von Importstatements (Frage vom letzten Mal)
https://stackoverflow.com/questions/20762662/whats-the-correct-way-to-sort-python-import-x-and-from-x-import-y-statement
* Stackoverflow ist eine ganz tolle Seite zum Lösen von Programmierproblemen =)
* Lest nicht nur die erste Antwort, sondern auch Kommentare, die Erklärung zur Frage und weitere Antworten!

### Hausaufgaben
* Der Lesestoff ist eher optional und zum Spaß haben, das kann zeitunahängig konsumiert werden

# Code aus dem Hedgedoc
https://md.ha.si/hSddjptRQKGKXFzatiQGRQ#
Erste Version Aufgabe 1
```
from turtle import *
import random

ecken = int(input("Wieviele Ecken dürfen es denn sein? "))

colormode(255)
pensize(10)
for i in range(ecken):
    pencolor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    fd(20)
    lt(360/int(ecken))

exitonclick()
```
Zweite Version; ohne random, mit Farbennamen
```
from turtle import *

ecken = int(input("Wieviele Ecken dürfen es denn sein? "))

farben = ["red", "green", "blue"]

while len(farben) < ecken:
    farben = farben + farben


pensize(10)
for i in range(ecken):
    pencolor(farben[i])
    fd(20)
    lt(360/int(ecken))

exitonclick()
```


Aufgabe 3.1
```
import random

rand1=random.randint(1,20)
rand2=random.randint(1,20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)
answer = input(inputfrage)
answer = int(answer)
    
if answer == rand1 + rand2:
    print("Richtig")
    
else:
    print("Falsch")
```




Aufgabe 3.2

```
import random

ANZAHL_VERSUCHE = 3

rand1=random.randint(1,20)
rand2=random.randint(1,20)
inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)

    
for i in range(ANZAHL_VERSUCHE):
    answer = input(inputfrage)
    answer = int(answer)
    if answer == rand1 + rand2:
        print("Richtig")
        break
    else:
        print("in zeile 89 ist i:", i)
        if i == ANZAHL_VERSUCHE - 1:
            print("Falsch! Das war dein letzter Versuch!")
            break
        print("Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i+1)))
        
```
