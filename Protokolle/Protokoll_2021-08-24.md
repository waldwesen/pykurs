
# Pikos Python Kurs 24.08.2021


Mitschrift 24.08.2021 
## Organisatorisches: 
 
Wenn Gruppensachen zu besprechen / klären sind – bitte Sandala kontaktieren – kümmert sich um Gruppensachen 
Kontakt E-Mail: sandala ätt gmx.net>

 
Auf Gitlab sind die Aufgaben und Mitschriften zu finden
https://gitlab.com/sudo_piko/pykurs
 
Mailingliste: Python – Orgaliste für den Pyhton-Anfängerinnen-Kurs 
Einschreibe-URL gibt es bei Piko
 
Es gibt eine Haecksenliste / Rocketchat – bei Interesse bitte Piko kontaktieren 
 
Info an uns:
Die perfekte Programmiererin kennt sich soweit aus, dass sie weiß nach welchen Keywords sie suchen muss. 
Wir sollen uns nicht stressen, wenn wir uns einen Teil nicht merken können. Wichtig ist, dass alles nachvollzogen werden kann. 
 
 
## Hausaufgaben Besprechung: 
 
### Aufgabe 1: Selbstbezüglichkeit
name = "Piko"
Dein Name ist 4 Buchstaben lang, Piko. Dieser Satz ist xxx Zeichen lang
 
Wie kann die Länge eines Wortes in einem Satz ausgegeben werden?
Dein Name ist 4 Buchstaben lang sollte ausgegeben werden.
 
```
>>> name = piko 
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'piko' is not defined
 
```
Die Fehlermeldung zeigt es ist nicht ausgeführt worden
Was stimmt hier nicht?
Ich muss dem Computer sagen es ist ein string
Piko ist nicht definiert.
Richtig: name = "Piko" oder 
              name = 'Piko'
 
Was ist in der Name der Variable? 
name
 

 
```
>>>print ("Dein Name ist 4 Buchstaben lang, Piko.")
Ist wenig generalisierbar,
Besser ist: 
>>> Dein Name ist.            Buchstaben lang,             .
>>> print(.         len(name) …..) 
Besser ist Dein Name ist: …..  
Dh. wir wollen print (….. längere strings
 
```
Siehe das Beispiel aus "Automate the boring stuff with phyton", Al Sweigart.
Die Auswertung des Langen strings. 
Das Beispiel mit der Verkettung – das Alter wird dort eingesetzt. 
Dann wird int ausgeführt….. (   74.      +) zwischen den Klammern ist eine Rechnung und wird ausgerechnet. Das müssen wir im Kopf andersherum machen. 
 
```
>>> alter = "74"
>>> print("Nächstes Jahr wirst du " + str(int(alter) + 1) + " sein.")
>>> print("Nächstes Jahr wirst du " + str(int("74" ) + 1) + " sein.")
>>> print("Nächstes Jahr wirst du " + str(  74        +1) + " sein.")
>>> print("Nächstes Jahr wirst du " + str(       75     ) + " sein.")
>>> print("Nächstes Jahr wirst du " +        "75"         + " sein.")
>>> print(  "Nächstes Jahr wirst du 75"            + " sein.")
>>> print(           "Nächstes Jahr wirst du 75 sein."     )
Nächstes Jahr wirst du 75 sein.
```
 
Wie kommen wir auf die 4 – die len Funktion? 
len(„piko“)
piko ist in der Variable name gespeichert. Jetzt gibt es eine Verkettung. Name ist len 
 

```
(print ( ….. len(name)….) 
>>> print("hallo" + 3 ) warum funktioniert das nicht? 
TypeError: can only concatenate str (not “int”) to str
```
Da: str und int. 
 
Der Operator + hat unterschiedliche Bedeutungen
```
>>> 3 +3 
6
setzt es zusammen. 
>>>"hal" + "lo" setzt es als "hallo" zusammen. 
```
 
Das was wir wollen ist "Hallo"  + "3". Es ist dann eine Ziffer und keine Zahl mehr. Man manipuliert wie Text und nicht wie Zahl. 
"hallo3"
"3" aber wenn wir nicht die 3 haben sondern  
```
>>>len(name) 
4
Können wir nicht "len(name)" sagen?– warum klappt das nicht? Warum ist es nicht das gleich wie 4? 
"len(name)" == "4" len name wird durch die „ seiner Bedeutung beraubt und wird ein Zeichen. Unser Computer wertet nicht aus was drinnen steht.
>>>print(len(name)) dann gibt er die 4 aus
>>>print("len(name)") was gibt er anstelle der vier aus? 
len(name) das ist für den Computer kein String, sondern ein Integer (Ganzzahl) 
```
 
Wir wollen von der len(name) auf die "4" (String) kommen. Wie können wir den string von len name bekommen? 
```
>>>str(len(name)) schon haben wir eine '4'.
```
 
" ' nicht vermischen . "4" das gleich wie'4' 
 
„Hallo4      
Int kann mit string nicht verkettet werden. 
Print("Dein Name ist " + ……… + " Buchstaben lang, " + …… ) Hier muss was passieren damit dort Piko steht
 
```
str(len(name)
>>> print("Dein Name ist " + str(len(name)) + "Buchstaben lang, " + name + "." ) 
>>>name = "kijetesantakalu"
>>>print( ("Dein Name ist " + str(len(name)) + " Buchstaben lang,  " + name + "." ) 
```
 
Wo ist der Unterschied zwischen + und , ?
```
>>>print("Hallo", "Piko"). 
```
Hier steht keine Rechnung. Kein Ausdruck den der Computer auswerten kann. Mit dem Komma wird eine Auflistung übergeben. Diese erhält die Funktion - 
Hallo Piko
 

```
>>>print("Hallo" + "Piko") 
HalloPiko
```
 
Hier geschieht eine Art Rechnung zwischen den Klammern. Ähnlich wie:
```
>>>print(3 + 5) 
```
Der Computer geht erst in die Klammer und fragt sich ist das eine Rechnung? Dann rechnet dieser die aus. Er rechnet 3+5 ist 8 und printet das dann
Liste von Parametern wird als Funktion ausgegeben. 
 
 
print(len(name)) Computer geht erst in die Klammer hinein.
4
 
+ macht alle Teile zu einem Parameter,
, macht eine Liste von Parametern.
, macht selber Leerzeichen dazwischen
 
```
>>>satz1 = "Dieser Satz ist Zeichen lang."
satz1 definiert 
>>>len(satz1) 
30
Wichtig die Unterscheidung Buchstaben und Zeichen
len zählt die Zeichen 
>>>len("…..") ? 
4 
```
Auch Leerzeichen sind Zeichen
 
Diesen Satz müssen wir aufteilen
```
>>>satzteil1 = „Dieser Satz ist‘
SyntaxError: E0L while scanning string literal
Er hat erwartet das der string mit einem doppelten „ endet. Kommt das nicht hört er damit nicht auf 
>>> satzteil1 = "Dieser Satz ist'"
>>> satzteil1
"Dieser Satz ist '"
 
>>>satzteil2 = „ Zeichen lang.“ 
```
Am Ende des Ergebnis dieser Satz ist….. wieviele Zeichen hat er? Je nachdem welche Zahl drinnen ist wird der Satz länger. Wäre die Zahl dreistellig, 100 oder mehr Buchstaben?. Dann wären es drei Zeichen – die dazwischen reingetan werden. Wenn dieser Satz weniger als 10 Buchstaben hätte, dann ein Zeichen. 
 
satzteil1 +  …zahl  +…. satzteil2
Oben ist keine zahl sondern? Str(zahl) es ist der string einer zahl. Sonst würden sich zahlen und strings nicht addieren lassen. 
 
Richtig 
```
>>>satzteil1 +  "." + satzteil2
"Dieser Satz ist ..Zeichen lang."
 
>>> laenge = len(satzteil1 + satzteil2) + 2
>>> laenge
32
 
>>>len(satzteil1 + satzteil2+ 2 warum funktioniert das nicht?
Weil str und int gemischt sind . Die Addition muss außerhalb der Klammer 
 
```
 
len zählt immer spaces. len("     ")
 
```
>>>laenge = len(satzteil1 + satzteil2 + "2")
31
 
>>>laenge = len(satzteil1 + satzteil2) + 2
32 
Satzteil 1 und Satzteil 2 sind strings. Die ergeben: Dieser Satz ist Zeichen lang… 
Len wertet die Länge des strings aus. Dieser Satz ist Zeichen lang
Auswertung 
 
>>>print(satzteil1 + laenge + satzteil2) was ist das Problem?
Str und int gemischt 
 
>>>print(satzteil1 + str(laenge) + satzteil2)
Dieser Satz ist 32 Zeichen lang.
```
 

 
### Aufgabe 2: 
Was sind
-Operatoren
-Variablen
-Funktionen
-Werte 
Dinge, die in einer Variable gespeichert werden können ("Werte"/"values")
 
Geteilte Notizen: 
 
·       Operatoren:+ - * / // %  + & < > %= != in not in
·       Variablen Namen: 
·       :  ausgabe
o   susi, strolch,  
o   a  hallo Zahl2  halloWelt

    n_int, b_bool, 

o    
Können durch funktionen getrennt werden. 
·        
·       Int bool string double
·       Funktionen:
·       print()
·       len()
·       max()
·       type()
o   hex(255) --> 0xff, konvertiert einen Integer zu einem Hexadezimal String
o   round(1.7)sagt was sie tut. Sie rundet
o    --> 2, es wird die gerundete Zahl ausgegeben
·       Werte was man in einer Variable abspeichern kann: int, float, datetime,str. 3
 
Laenge = len(Ausdruck) 
 
Laenge  32 ist der Wert der Variable 
 
 
## Funktionen 
Woran erkennt man eine Funktion? 
 
Was ist das Destillat der Syntax einer Funktion? 

```
name_der_funktion(argument)

```
() es sind die Ärmchen die etwas entgegen nehmen oder nicht.
 
Manchmal ist nichts in der Klammer. Dann weiß die Funktion was sie machen soll.
 
Funktionen haben vier Anteile. 
l = len(„Dies ist ein Satz“.“)
len ist die Funktion
Ärmchen umschließen die Argumente der Funktion
Es fällt etwas raus. in l ist die Ausgabe der Funktion aufbewahrt. 
Manche Funktionen machen noch etwas anderes.
Wie z.b. print(„Dies ist ein Satz“)
Print hat einen Effekt, dass etwas ausgegeben wird
len rechnet es aus was die Länge ist, es hat nicht den Effekt das etwas ausgegeben wird
p = print(„Dies ist ein Satz“) 
Ich verwende print wie ich  len verwendet habe. Was passiert hier? Was sind die Auwirkungen dieser Zeile? Die Funktion wird ausgeführt.  
l = 18
p  ist nichts gepeichert 
 
print(p)
None. !!! 
Ist wichtig. Das gibt es ist aber nicht drinnen. 
 
NameError: Name 
 
Es gibt eine Funktion die machen etwas, solche die etwas ausgeben oder die keinen Rückgabewert haben:
 
```
>>> type(p)
<class 'NoneType'>
>>> type(l)
<class 'int'>
```
 
Mit nicht Existenzen mit Computern umgeht. Da gibt es viele Nerdwitze
 
Funktionen die etwas zurückgeben, sind zb len . das Ergebnis kann man in einer Variable speichern.
 
Es gibt Funktionen die einen Effekt und etwas zurückgeben: 
 
Reihenfolge
len("hallo")  ist 5 
len(„Auf Wiedersehen“) ist mehr als 5 (15) 
 
Verkettung von Funktionen. 5 und 15 wird verglichen und 15 gewinnt:
 
len(max("Hallo", "Auf Wiedersehen")) ist auch eine Verkettung. Ich bilde das Maximum aus beiden Wörtern und davon dann die menge
 
 
Max bei Buchstaben – Was ist für die max Funktion größer, „hallo“ oder „aufwiedersehen“? 
Hallo! Das H ist größer. Erst wird die innere Funktion ausgeführt. Hallo wird mit auf Wiedersehen verglichen. Das H gewinnt. Die max Funktion gibt hallo aus und darauf wird len verwendet. 5 
 
! Immer von Innen nach Außen ausrechnet. Die Reihenfolgen sind sehr wichtig
 
 ## Orgakrams
Bitte schaut morgen in das gitlab
Die Aufgaben von heute sind ab Morgen 25.08.2021 unter Gitlab zu finden 


# Pikos Idle

```


>>> name = "Piko"
>>> name = Piko
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'Piko' is not defined
>>> name
'Piko'
>>> print("Dein Name ist 4 Buchstaben lang, Piko.")
Dein Name ist 4 Buchstaben lang, Piko.
>>> Dein Name ist      Buchstaben lang,      .
KeyboardInterrupt
>>> print( ... len(name) .... ) 
KeyboardInterrupt
>>> print("hallo" + 3)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
>>> 3 + 3
6
>>> "hal" + "lo"
'hallo'
>>> "hallo" + "3"
'hallo3'
>>> len(name)
4
>>> print(len(name))
4
>>> print("len(name)")
len(name)
>>> str(len(name))
'4'
>>> "hallo" + str(len(name))
'hallo4'
>>> "hallo" + len(name)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
>>> print("Dein Name ist " + str(len(name)) + " Buchstaben lang, " + name + "." )
>>> print("Dein Name ist " + str(len(name)) + " Buchstaben lang, " + name + "." )
KeyboardInterrupt
>>> name = "kijetesantakalu"
>>> print("Dein Name ist " + str(len(name)) + " Buchstaben lang, " + name + "." )
Dein Name ist 15 Buchstaben lang, kijetesantakalu.
>>> print("Hallo", "Piko")      print("Hallo" + "Piko")  print(3 + 5) 
KeyboardInterrupt
>>> satz1 = "Dieser Satz ist  Zeichen lang."
>>> satz1
'Dieser Satz ist  Zeichen lang.'
>>> len(satz1)
30
>>> len("    ")
4
>>> satzteil1 = "Dieser Satz ist '
  File "<stdin>", line 1
    satzteil1 = "Dieser Satz ist '
                                  ^
SyntaxError: EOL while scanning string literal
>>> satzteil1 = "Dieser Satz ist '"
>>> satzteil1
"Dieser Satz ist '"
>>> satzteil1 = "Dieser Satz ist "
>>> satzteil2 = " Zeichen lang."
>>> satzteil1 + ".." + satzteil2 
'Dieser Satz ist .. Zeichen lang.'
>>> print(satzteil1 + satzteil2 + "2")
Dieser Satz ist  Zeichen lang.2
>>> laenge = len(satzteil1 + satzteil2) + 2 
>>> laenge
32
>>> print(satzteil1 + str(laenge) + satzteil2)
Dieser Satz ist 32 Zeichen lang.
>>> l = len("Dies ist ein Satz.")
>>> l
18
>>> print("Dies ist ein Satz")
Dies ist ein Satz
>>> p = print("Dies ist ein Satz")
Dies ist ein Satz
>>> l
18
>>> p
>>> l
18
>>> p
>>> print(l)
18
>>> print(p)
None
>>> print(q)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'q' is not defined
>>> max(len("Hallo"), len("Auf Wiedersehen"))
15
>>> len(max("Hallo", "Auf Wiedersehen"))
5

```