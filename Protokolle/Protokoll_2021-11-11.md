# Pikos Python Kurs am 11.11.2021


# Hagzussas Mitschrift
haecksen-Termine und Hinweise:

15.11.: Basics-Workshop am  -> Info an piko per Mail bei Teilnahmewunsch, Mitbringen: Badeente oder Kuscheltier
2.12.: voraussichtlich Git-Workshop oder anderes Thema

cfp für haecksen-Programm rc3: https://pretalx.c3voc.de/rc3-2021-haecksen/cfp

15.11. 18:00 Uhr: Treffen Podcast-Kollektiv
montags 18:30 Uhr: Bau der World
dienstags 19:00 Uhr: Orga-TReffen
12.11.: 20:00 Uhr Vereinsgründung


(Tipp von chinah:
https:\\replit.com)

Hausaufgaben:
## Aufgabe 1

Ideen für docstrings hier sammeln:
https://md.ha.si/6TkBVKlISy6zg272KVia1A

Mehrzeilige Docstrings - richtige Schreibweise in Doku nachlesen
Sprache anhand Zielgruppe wählen, auch bei Benennung von Variablen und Parametern


## Aufgabe 2

print-Statement für Ausgabe nicht vergessen

While-Schleife für elegante Lösung verwenden

Variante mit For-Schleife und großer Range auch möglich, aber Bugmaterial

Tabs vs. Spaces sind Fehlerquellen

len(liste) beginnt bei 1 zu zählen

Sylvias Code:

primliste100 = []

while len(primliste100) <= 100:
for m in range(600):
if prim(m): # wenn prim(m) true
primliste100.append(m)

print(primliste100[:100])

julikas code:

p = 0
while len(primliste100) <= 100:
    p+=1    # p = p+1
    if prim(p): # wenn prim(p) true
        primliste100.append(p)

elises Code:

primlistebis100 = []

for m in range(10000000):
if prim(m):
anzahl = anzahl + 1
primlistebis100.append(m)
elif anzahl == 100:
break

# for (i = 0; i <= 100; i++)

print(primlistebis100)
print(anzahl)

Default-Werte in eigenen Funktionen definieren, können später beim Aufruf so verwendet oder verändert werden
Positional argument: Werte werden in zuvor in Funktionsdefinition festgelegter Reihenfolge verarbeitet
Parameter mit Default-Wert müssen hinten stehen!

~~~~~~~~~~~~~~~~~~~~~~~~~~

Aufgabe 3 vom letzten Mal:
For-Schleife ersetzt wiederholende Aufrufe

Kapseln von mehrfach ausgeführten Codeteilen = Defninieren weiterer Funktionen

pu(), pd() einfügen


Erstes und letztes Element einer Liste ausgeben: liste[-1]


~~~~~~~~~~~~~~~~~~~~~~~~~~

Spielerei von naerrin - Zeichnen einer Turtle
~~~
from turtle import *

pu()
setposition(0, 400)
pd()
pensize(10)
color("green", "green")
begin_fill()
#start kopf
lt(225)
fd(100)
lt(65)
fd(140)
rt(70)
fd(160)
rt(80)
#start linkes vorderbein
fd(120)
lt(60)
fd(80)
lt(110)
fd(150)
rt(50)
#start seite)
fd(140)
lt(32)
fd(140)
rt(70)
#start linkes hinterbein
fd(140)
lt(95)
fd(95)
lt(100)
fd(110)
#start hintern
rt(80)
fd(176)
setposition(0, 400)
#start kopf
rt(22)
fd(100)
rt(65)
fd(140)
lt(70)
fd(160)
lt(80)
#start linkes vorderbein
fd(120)
rt(60)
fd(80)
rt(110)
fd(150)
lt(50)
#start seite)
fd(140)
rt(32)
fd(140)
lt(70)
#start linkes hinterbein
fd(140)
rt(95)
fd(95)
rt(100)
fd(110)
#start hintern
lt(80)
fd(176)
end_fill()
exitonclick()
~~~


# Pikos Datei

## Hausaufgaben-Besprechung I

~~~
import math

def prim(zahl):
    """teste, ob eine ganze Zahl eine Primzahl ist"""
    liste = [2, 3, 5]
    if zahl < 2:  # keine Primzahl kleiner zwei
        return False
    elif zahl in liste:  # Liste ist mit Primzahlen
        return True
    for t in range(2, int(math.sqrt(zahl) + 1)):  # t ist alles von 2 bis Wurzel der Zahl
        if (zahl % t) == 0:  # dann wäre ja zahl durch t teilbar
            return False
    return True


primliste100 = []

# # Alte Version:
# for m in range(100):
#     if prim(m): # wenn prim(m) true
#         primliste100.append(m)

# Neue Version:
p = 0
while len(primliste100) < 100:
    p += 1
    if prim(p): # wenn prim(m) true
        primliste100.append(p)

print(len(primliste100))
~~~

## Default-Argumente

~~~
from turtle import

def buntes_dreieck(laenge=100, strichdicke=4, farbe="red", fuellfarbe="blue", hintergrund="green"):
    color(farbe, fuellfarbe)
    pensize(strichdicke)
    bgcolor(hintergrund)
    begin_fill()
    for i in range(3):
        fd(laenge)
        lt(120)
    end_fill()
    
~~~


## Hausaufgaben-Besprechung II

~~~
def rechteck_runde_ecken(laenge1, laenge2, entfernung, radius):
    pu()
    fd(entfernung)
    pd()

    for i in range(2):
        fd(laenge1)
        circle(radius, 90)
        fd(laenge2)
        circle(radius, 90)
    pu()
    bk(entfernung)
    pd()

# rechteck_runde_ecken(100, 200, 50, 10)



def list_ends(a_list):
    """erstes und letztes Element einer Liste ausgeben"""
    return [a_list[0], a_list[-1]]

liliste = ["eins", "zwei", "drei"]
print(len(liliste)) # 3

# Wie kommen wir an "drei"?

print(liliste[0], liliste[1], liliste[2])
# print(liliste[3])  # Ergibt eine Fehlermeldung.

~~~