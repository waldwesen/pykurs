# Pikos Python Kurs 26.10.2021

## waldwesens Protokoll

### Organisatorisches
Links:

* Gitlab (Aufgaben und Protokolle)
https://gitlab.com/sudo_piko/pykurs

* Mailingliste
https://lists.haecksen.org/listinfo/python

* Doodle für Extrastunde (für alle denen der Kurs zu schnell geht und die nochmal eine zusammengefasste Runde Theorie brauchen)
https://nuudel.digitalcourage.de/mkhkP1eEJbPapQgKxnflq79B/admin

* Kontakt zu Piko: Python@ithea.de

* Gruppen-Pad: https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?view# gerne zur Selbstorganisation nutzen!

### Termine
Das hier ist der letzte Dienstags-Termin, ab November (das ist nächste Woche der 4.11.!) dann Donnerstags-Termine!

### Tipps zur Herangehensweise an Aufgaben
* in den Code aus der letzten Stunde gehen, es ist völlig ok, Code einfach erstmal zu kopieren! =)
* von dort aus losprobieren

### Unterschied input und Variablen
numinput und input fragen die User*in nach einem input, Variablen werden im Programm selbst festgestellt, es ist wichtig das zu trennen.
Beim Überlegen für eine Lösung immer darüber nachdenken, ob ihr eine Information von der User*in braucht, oder ob die Information vom Programm selbst herausgefunden werden soll.
Beispiel Pizza: eine Funktion macht alleine Pizza, mit dem was im Kühlschrank ist, kein Input nötig. Oder die Funktion fragt nochmal nach, was für eine Pizza die User*in möchte (dann braucht es input ;))

### Was sollen diese Doppelpunkte? (siehe Lösung für Aufgabe 3)
* variable1[4] - gibt nur den 4. Buchstaben aus
* variable1[:4] - gibt alles bis zum  4. Buchstaben aus
* variable1[::2] - gibt nur jeden 2. Buchstaben aus
* variable1[1:10:2] - fängt erst beim 2. Buchstaben an, läuft bis zum 10. Buchstaben und springt dabei immer 2 weiter
...am besten selbst Ausprobieren ;)

### Kommentare
* In Pycharm könnt ihr euch Kommentare mit # an/in euren Code schreiben, nutzt das gerne!
* In der Fachsprache heißt das auch "Auskommentieren" in jeder IDE sollte es auch einen shortcut dafür geben
* der default in pycharm ist STRG+/, (das / auf dem Nummernblock nutzen, 7 klappt nicht), das kann man auch in den Einstellungen umstellen

### Haecksen-Termine
* Samstag, 30.10. 14:00 World Mappen: Einführung und Start (in Ada https://meeten.statt-drosseln.de/b/cri-uye-ogv-vfh)



# Pikos Datei

~~~
from turtle import *

def dreieck(laenge):
    for i in range(3):
        fd(laenge)
        rt(120)


def dreieck_mit_dicke(laenge, dicke):
    pensize(dicke)
    for i in range(3):
        fd(laenge)
        rt(120)


def jump(laenge):
    pu()
    fd(laenge)
    pd()

FESTLEGUNG = 100

def jump_festgelegt():
    pu()
    fd(FESTLEGUNG)
    pd()


# dreieck(130)
# jump(200)
# dreieck(50)
# done()


## Kombiniert mit input:
# seitenlaenge = int(input("Wie lange soll die Länge sein? "))
# dreieck(seitenlaenge)
~~~

~~~
zahl1 = 7
zahl2 = 9
zahl3 = 15


# print(3 == 7) # == wertet zu True oder False aus.

def ist_das_die_summe(summand1, summand2, summe):
    if summand1 + summand2 == summe:
        return True
    else:
        return False

# Kürzere Variante, die das Gleiche tut:
def ist_das_die_summe2(summand1, summand2, summe):
    return summand1 + summand2 == summe


# print(ist_das_die_summe2(zahl1, zahl2, zahl3))  # => True/False


# a = 3
# b = 5
# c = int(input("Was ist drei plus fünf? "))
# print(ist_das_die_summe2(a, b, c))
~~~


~~~
# VERSION 3 des Matheübungsprogramms: mit der Funktion ist_das_die_summe
# import random
#
# ANZAHL_VERSUCHE = 3
# ANZAHL_AUFGABEN = 4
# SCHWIERIGKEITSGRAD = 10
#
# punkte = 0
#
#
# for j in range(ANZAHL_AUFGABEN):
#     rand1 = random.randint(1, SCHWIERIGKEITSGRAD)
#     rand2 = random.randint(1, SCHWIERIGKEITSGRAD)
#     inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)
#
#
#     for i in range(ANZAHL_VERSUCHE):
#         answer = input(inputfrage)
#         answer = int(answer)
#         if ist_das_die_summe2(rand1, rand2, answer):  # vorher: answer == rand1 + rand2:
#             print("Richtig")
#             punkte = punkte + 1
#             break
#         else:
#             if i == ANZAHL_VERSUCHE - 1:
#                 print("Falsch! Das war dein letzter Versuch!")
#                 break
#             if abs(answer - (rand1+rand2)) == 1:
#                 print("Du liegst nur um 1 daneben!")
#             print("Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i + 1)))
#
#
#     print("Aktueller Punktestand: " + str(punkte))
~~~


# AUFGABE 3
~~~
def rueckwaerts(wort):  # wort ist abcdef
    ausgabe = ""  # soll am Ende "fedcba" sein.
    while len(wort) > 0:
        # print(wort, ausgabe) # zum besseren Verständnis
        ausgabe = ausgabe + wort[-1]
        wort = wort[:-1]
    return ausgabe

def drehen_und_oft_ausgeben(wort, anzahl):
    gedreht = rueckwaerts(wort)
    print(gedreht*anzahl)


# drehen_und_oft_ausgeben("haecksen", 5)
~~~

# Kurze Erklärung zu eckigen Klammern bei Strings
~~~
variable1 = "HALLOhallo"
print(variable1[4], variable1[:4], variable1[::2])
~~~